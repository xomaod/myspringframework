package com.odddd333.springframework.demo.repository;

import com.odddd333.springframework.demo.domains.active.Userdetails;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserDetailsRepository extends JpaRepository<Userdetails, String> {

}
