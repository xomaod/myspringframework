package com.odddd333.springframework.demo.repository;

import com.odddd333.springframework.demo.domains.active.Comment;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface CommentRepository extends JpaRepository<Comment, Integer> {
    @Query("SELECT c FROM Comment c WHERE c.comment LIKE %?1%")
    public List<Comment> searchComment(String comment);
}

