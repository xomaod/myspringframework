package com.odddd333.springframework.demo.repository;

import com.odddd333.springframework.demo.domains.active.Profile;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProfileRepository extends JpaRepository<Profile, Integer> {
}
