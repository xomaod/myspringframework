package com.odddd333.springframework.demo.repository;

import com.odddd333.springframework.demo.domains.active.Topic;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TopicRepository extends JpaRepository<Topic, Integer> {

}
