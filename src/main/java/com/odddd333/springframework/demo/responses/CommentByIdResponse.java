package com.odddd333.springframework.demo.responses;


import com.fasterxml.jackson.annotation.JsonProperty;

public class CommentByIdResponse {
    @JsonProperty(value = "id")
    private int id;

    @JsonProperty(value = "comment")
    private String comment;

    @JsonProperty(value = "author")
    private String author;

    public CommentByIdResponse(int id, String comment, String author){
        this.id = id;
        this.comment = comment;
        this.author = author;
    }

    public long getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }
}
