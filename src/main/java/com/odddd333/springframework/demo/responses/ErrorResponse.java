package com.odddd333.springframework.demo.responses;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ErrorResponse {

    @JsonProperty(value = "code")
    private long code;

    @JsonProperty(value = "message")
    private String message;

    public ErrorResponse(long code, String message){
        this.code = code;
        this.message = message;
    }

}
