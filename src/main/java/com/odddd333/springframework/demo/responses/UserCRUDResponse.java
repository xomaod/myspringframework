package com.odddd333.springframework.demo.responses;

import com.fasterxml.jackson.annotation.JsonProperty;

public class UserCRUDResponse {

        @JsonProperty(value = "code")
        private long code;

        @JsonProperty(value = "message")
        private String message;

        public UserCRUDResponse(long code, String message){
            this.code = code;
            this.message = message;
        }


        public long getCode() {
            return code;
        }

        public void setCode(long code) {
            this.code = code;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

}
