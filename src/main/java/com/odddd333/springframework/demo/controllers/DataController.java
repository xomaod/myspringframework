package com.odddd333.springframework.demo.controllers;

import com.odddd333.springframework.demo.domains.active.Comment;
import com.odddd333.springframework.demo.domains.active.User;
import com.odddd333.springframework.demo.domains.active.UserProfile;
import com.odddd333.springframework.demo.responses.CommentCRUDResponse;
import com.odddd333.springframework.demo.responses.UserProfileCRUDResponse;
import com.odddd333.springframework.demo.services.CommentService;

import com.odddd333.springframework.demo.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.validation.ObjectError;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@RestController
public class DataController {

    @Autowired
    private CommentService commentService;

    @Autowired
    private UserService userService;

    //@Autowired
    //private UserProfileService userProfileService;


    /*====================== User / Profile Entity ============================*/
    //List All
    @GetMapping("/user")
    public List<User> getUsers(){
        return userService.getUsers();
    }

    //Insert
    @PostMapping("/userprofile")
    public UserProfileCRUDResponse postUser(@Validated(UserProfile.New.class) @RequestBody UserProfile userProfile){
        //return userProfileService.postUserProfile(userProfile);
        return new UserProfileCRUDResponse(200, "success");
    }

    /*====================== Comment Entity ============================*/
    //List All
    @GetMapping("/comment")
    public List<Comment> getComments(){
        return commentService.getComments();
    }

    //Get by Id
    @GetMapping("/comment/{id}")
    public Optional<Comment> getCommentbyId(@PathVariable(value = "id", required = true) int id) throws Exception{
        return commentService.getCommentById(id);
    }

    //Insert
    @PostMapping("/comment")
    public CommentCRUDResponse postComment(@Validated(Comment.New.class) @RequestBody Comment comment){
        return commentService.postComment(comment);
    }

    //Search
    @PostMapping("/comment/search")
    public List<Comment> searchComment(@RequestBody Comment comment){
        return commentService.searchComment(comment);
    }

    //Update
    @PutMapping("/comment/{id}")
    public CommentCRUDResponse putComment(@Validated(Comment.Existing.class) @PathVariable(value = "id", required = true) int id, @RequestBody Comment comment){
        return commentService.putComment(id, comment);
    }

    //Delete
    @DeleteMapping("/comment/{id}")
    public CommentCRUDResponse deletetComment(@PathVariable(value = "id", required = true) int id){
        return commentService.deleteComment(id);
    }


    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public List<String> handleValidationExceptions(MethodArgumentNotValidException ex) {
        return ex.getBindingResult()
                .getAllErrors().stream()
                .map(ObjectError::getDefaultMessage)
                .collect(Collectors.toList());
    }

}
