package com.odddd333.springframework.demo.controllers;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloWorld {

    @RequestMapping("/")
    public String indexController(){
        return "OK";
    }

    @RequestMapping("/hello")
    public String helloWorldController(){
        //Test Debugging
        int j = 0;

        for(int i = 0; i < 20; i++){

            j += i;

        }

        return "Hello" + j;
    }
}

