package com.odddd333.springframework.demo.services;
import com.odddd333.springframework.demo.domains.active.Comment;
import com.odddd333.springframework.demo.repository.CommentRepository;
import com.odddd333.springframework.demo.responses.CommentCRUDResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Optional;

@Service
public class CommentService {

    @Autowired
    private CommentRepository commentRepository;


    public List<Comment> getComments(){

        return commentRepository.findAll();
    }

    public Optional<Comment> getCommentById(int id){
        return commentRepository.findById(id);
    }

    public Boolean getExistingCommentById(int id){
        return commentRepository.existsById(id);
    }

    public CommentCRUDResponse postComment(Comment comment){
        if(comment.getId() > 0){
            return new CommentCRUDResponse(400, "Bad request.");
        }else {
            commentRepository.save(comment);
            return new CommentCRUDResponse(200, "Insert complete.");
        }
    }

    public List<Comment> searchComment(Comment comment){
        return commentRepository.searchComment(comment.getComment());
    }
    public CommentCRUDResponse putComment(int id, Comment comment){

        if(!this.getExistingCommentById(id)){
            return new CommentCRUDResponse(404, "Id not found.");
        }else {

            Optional<Comment> findCommentById = this.getCommentById(id);
            Comment existingComment = findCommentById.get();

            comment.setComment((comment.getComment() == null) ? existingComment.getComment() : comment.getComment());
            comment.setAuthor((comment.getAuthor() == null) ? existingComment.getAuthor() : comment.getAuthor());
            comment.setId(id);
            commentRepository.save(comment);

            return new CommentCRUDResponse(200, "Update complete.");
        }
    }

    public CommentCRUDResponse deleteComment(int id){
        if(!this.getExistingCommentById(id)){
            return new CommentCRUDResponse(404, "Id not found.");
        }else {
            commentRepository.deleteById(id);
            return new CommentCRUDResponse(200, "Delete complete.");
        }
    }

}
