package com.odddd333.springframework.demo.services;


import com.odddd333.springframework.demo.domains.active.Profile;
import com.odddd333.springframework.demo.domains.active.User;
import com.odddd333.springframework.demo.repository.ProfileRepository;
import com.odddd333.springframework.demo.repository.UserRepository;
import com.odddd333.springframework.demo.responses.UserCRUDResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserService {

    @Autowired
    UserRepository userRepository;

    @Autowired
    ProfileRepository profileRepository;

    public UserCRUDResponse postUser(User user){

        userRepository.save(user);
        //profileRepository.save(profile);

        return new UserCRUDResponse(200, "Success");

    }

    public List<User> getUsers(){
        return userRepository.findAll();
    }
}
