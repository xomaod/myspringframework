package com.odddd333.springframework.demo.domains.active;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.*;

@Entity
public class User {

    @Id
    @GeneratedValue
    @JsonProperty(value = "id")
    private int id;

    @Column
    @JsonProperty(value = "account")
    private String account;

    @Column
    @JsonProperty(value = "password")
    private String password;

    @OneToOne(cascade = CascadeType.ALL)
    private Profile profile;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", account='" + account + '\'' +
                ", password='" + password + '\'' +
                ", profile=" + profile +
                '}';
    }
}
