package com.odddd333.springframework.demo.domains.active;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.*;
import java.util.Date;

@Entity
public class Profile {

    @Id
    @GeneratedValue
    @JsonProperty(value = "id")
    private int id;

    @OneToOne
    @PrimaryKeyJoinColumn
    private User user;

    @Column
    @JsonProperty(value = "name")
    private String name;

    @Column
    @JsonProperty(value = "date_of_birth")
    @Temporal(TemporalType.DATE)
    private Date date_of_birth;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getDate_of_birth() {
        return date_of_birth;
    }

    public void setDate_of_birth(Date date_of_birth) {
        this.date_of_birth = date_of_birth;
    }
}
