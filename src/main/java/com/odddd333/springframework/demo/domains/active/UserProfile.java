package com.odddd333.springframework.demo.domains.active;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.Column;
import javax.validation.constraints.NotNull;
import java.util.Date;

public class UserProfile {

    @JsonProperty(value = "account")
    @NotNull(message = "Please define account.", groups = New.class)
    private String account;

    @JsonProperty(value = "password")
    @NotNull(message = "Please define password.", groups = New.class)
    private String password;

    @JsonProperty(value = "name")
    @NotNull(message = "Please define your name.", groups = New.class)
    private String name;

    @JsonProperty(value = "date_of_birth")
    private Date date_of_birth;

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getDate_of_birth() {
        return date_of_birth;
    }

    public void setDate_of_birth(Date date_of_birth) {
        this.date_of_birth = date_of_birth;
    }

    public interface New{}
}
