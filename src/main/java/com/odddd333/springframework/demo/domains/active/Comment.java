package com.odddd333.springframework.demo.domains.active;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Null;

@Entity
public class Comment {

    @Id
    @GeneratedValue
    @JsonProperty(value = "id")
    private int id;

    @Column
    @JsonProperty(value = "comment")
    //@JsonInclude(JsonInclude.Include.NON_NULL) ===> In case to not display in JSON if this field is Null
    @NotNull(message = "Please enter your comment.", groups = New.class)
    private String comment;

    @Column
    @JsonProperty(value = "author")
    //@JsonInclude(JsonInclude.Include.NON_NULL) ===> In case to not display in JSON if this field is Null
    @NotNull(message = "Please enter your author.", groups = New.class)
    private String author;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }


    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public interface New {}
    public interface Existing {}
}
