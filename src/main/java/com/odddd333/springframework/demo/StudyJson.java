package com.odddd333.springframework.demo;


import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class StudyJson {

    @RequestMapping(value = "/json", method = RequestMethod.GET, produces = "application/json")
    public String getJson(){
        return "{  \"response\" : \"your string value\" }";
    }

    @RequestMapping("/json/jackson")
    public String getJsonJackson(){
        return "Jackson";
    }
}
